import './App.css'
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import {Menu} from './components/Menu'
import { ListadoUsuarios } from './components/ListadoUsuarios';
import { ListadoFotos } from './components/ListadoFotos';
import { ListadoComentarios} from './components/ListadoComenterios';
import { useState } from 'react';
import {
  BrowserRouter as Router, Route, Link, Routes} from "react-router-dom";

function App() {

  let [opcion,setOpcion]=useState('usuarios');

  return (
    <div className="App container">
      <div className='py-4'>
        <Router>
          <Menu/>
          <h1 className='text-center p-2'>Hello CAR IV</h1>
          <Routes>
            <Route path='/' element={<ListadoUsuarios/>}/>
            <Route path='/fotos' element={<ListadoFotos/>}/>
            <Route path='/comentarios' element={<ListadoComentarios/>}/>
          </Routes>
        </Router>
      </div>
    </div>
  )
}

export default App
