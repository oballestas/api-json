export function Comentario({nombre, contenido}){
    return (
        <div className="col-4 pb-3">
            <div className="card" style={{width: '12rem'}}>
                <div className="card-body">
                    <h5 className="card-title">{nombre}</h5>
                    <p className="card-text">{contenido}</p>
                </div>
            </div>
        </div>
    )
}