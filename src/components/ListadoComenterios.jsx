import {Section} from './Section'
import {Comentario} from './Comentario'
import { useEffect, useState } from 'react'

export function ListadoComentarios () {

    let [comentario,setComentario]=useState(null);
    const URL_COMENTARIOS='https://jsonplaceholder.typicode.com/comments';
        
    //Datos de los comentarios
    useEffect ( ()=>{
  
      fetch(URL_COMENTARIOS)
        .then(response => response.json())
        .then(datos => 
          setComentario(datos)
          )
        },[]);

    return (
        <Section titulo='Comentarios'>
            <div className='container'>
                <div className='row col-12'>
                {comentario? comentario.slice(0,21).map((comentario)=>{
                    return (
                        <Comentario 
                        key={comentario.name}
                        nombre={comentario.name}
                        contenido={comentario.body}/>
                        )
                    }):<h3>Cargando comentarios...</h3>
                    }
                </div>
            </div>
      </Section>
    )
}