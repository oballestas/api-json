import { Link } from "react-router-dom";

export function Menu(){
    return (
        <div>
            <ul className="nav">
                <li className='nav-item'>
                    <Link className="nav-link" to='/'>
                        Usuarios
                    </Link>
                </li>
                <li className='nav-item' >
                    <Link className="nav-link" to='/fotos'>
                        Fotos
                    </Link>
                </li>
                <li className='nav-item'>
                    <Link className="nav-link" to='/comentarios'>
                        Comentarios
                    </Link>
                </li>
            </ul>
        </div>
    )
}